import React from 'react';
import MenuComponent from '../components/MenuComponent';
import PhotoComponent from '../components/PhotoComponent';

const Photo = () => {

    return (
        <MenuComponent>
            <PhotoComponent />
        </MenuComponent>
    );
};

export default Photo;