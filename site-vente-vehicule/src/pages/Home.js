import React from 'react';
import MenuComponent from '../components/MenuComponent';
import HomeComponent from '../components/HomeComponent';

const Home = () => {
  return (
    <div>
        <MenuComponent>
          <HomeComponent />
        </MenuComponent>
    </div>
  );
};

export default Home;
