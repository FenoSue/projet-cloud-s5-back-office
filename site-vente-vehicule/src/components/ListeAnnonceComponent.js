import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import '../css/ListeCss.css';

const ListeAnnonceComponent = () => {
    const navigate = useNavigate();
    const [listeAnnonces, setListeAnnonces] = useState([]);
    const [idAnnonce, setIdAnnonce] = useState('');

    useEffect(() => {
        getListeAnnonce();
    }, []); 

    const getListeAnnonce = async () => {
        try {
            const response = await fetch('http://localhost:8080/ListeAnnonceNonValider', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            });
        
            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                const data = await response.json();
        
                if(data.status===200) {
                    setListeAnnonces(data.data);
                }
                else if(data.status===400) {
                    console.log("erreur : "+data.data);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    };

    const getPhotos = async (annonceId) => {
        navigate(`/detailPhotos/${annonceId}`)
    };

    const validerAnnonce = async (idAnnonce) => {
        console.log('Validation ok!')
        console.log('idannonce : '+idAnnonce);
        const token = localStorage.getItem('token');
        try {
            const response = await fetch('http://localhost:8080/ValiderAnnonce', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    id:idAnnonce,
                }),
            });
        
            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                const data = await response.json();
        
                if(data.status===200) {
                    window.location.reload();
                }
                else if(data.status===400) {
                    console.log("erreur : "+data.data);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    };

    return (
        <div>
        <h4>Listes des annonces</h4>
        <hr></hr>
        <div className='contenu'>
            <table className="table table-bordered custom-table">
                <thead>
                <tr>
                    <th>réf</th>
                    <th>Annonce</th>
                    <th>Valider</th>
                </tr>
                </thead>
                <tbody>
                    {listeAnnonces && listeAnnonces.map((annonce) => (
                    <tr key={annonce.id}>
                        <td>
                        <p>{annonce.id}</p>
                        </td>
                        <td>
                        <p>Fait le {annonce.dateHeure}</p>
                        <p>
                            Description : c'est une voiture {annonce.categorie} {annonce.couleur}. {annonce.nbrPlace} {annonce.description}. 
                            De marque {annonce.marque}, modèle {annonce.modele}. 
                            Avec une {annonce.boiteVitesse}. Sortie en {annonce.anneeSortie}
                        </p>
                        <p>
                            Carburant : {annonce.typeCarburant}
                        </p>
                        <a onClick={() => getPhotos(annonce.id)} className='lien-voir' style={{cursor: 'pointer'}}>Voir les photos</a>
                        </td>
                        <td>
                        <input 
                            type="checkbox"
                            value={annonce.id}
                            onClick={() => validerAnnonce(annonce.id)}
                        />
                        </td>
                    </tr>
                    ))}
                </tbody>
            </table>
        </div>
    </div>
    );
};

export default ListeAnnonceComponent;